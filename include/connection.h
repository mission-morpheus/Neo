#pragma once

#include "node.h"
#include <QHash>
#include <QJsonObject>
#include <QObject>

/*!
   \class Connection
 * \brief The Connection class manages connections between two nodes.
 */
class Connection : public QObject
{
  Q_OBJECT

  Q_PROPERTY(Node* receiver READ receiver WRITE setReceiver NOTIFY
               receiverChanged) /*!< \see Connection::receiver */
  Q_PROPERTY(Node* sender READ sender WRITE setSender NOTIFY
               senderChanged) /*!< \see Connection::sender */
public:
  /*!
   * \brief Create a connection instance.
   * \param parent The parent of the object to create.
   * \post The methods Connection::setReceiver and Connection::setSender should
   * be used right after creating the instance to initialize its members.
   */
  explicit Connection(QObject* parent = nullptr);

  /*!
   * \brief Get the node that acts as receiver in the connection.
   * \return a pointer to the node acting as receiver.
   */
  Node* receiver() const;

  /*!
   * \brief Get the node that acts as sender in the connection.
   * \return a pointer to the node acting as sender.
   */
  Node* sender() const;

  /*!
   * \brief Set the node that acts as receiver in the connection.
   * \param node The Node pointer to set as receiver.
   */
  void setReceiver(Node* node);

  /*!
   * \brief Set the ndoe that acts as sender in the connection.
   * \param node The node pointer to set as sender.
   */
  void setSender(Node* node);

  /*!
   * \brief Read a json object and update the members of the connection
   * accordingly. \param json The json object to be read. \param hash Hashmap
   * linking an unique 64 bit integer id to a Node pointer.
   */
  void read(const QJsonObject& json, const QHash<qulonglong, Node*>& hash);

  /*!
   * \brief Serialize the content of the connection in a json object.
   * \return The json object in which the connection have been written.
   */
  QJsonObject write() const;

signals:
  void
  receiverChanged();    /*!< Notify the node acting as receiver have changed. */
  void senderChanged(); /*!< Notify the ndoe acting as sender have changed. */

private:
  Node* m_receiver; /*!< Node acting as receiver. */
  Node* m_sender;   /*!< Node acting as sender. */
};
